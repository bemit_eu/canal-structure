<?php
/**
 * @param \Flood\Canal\Feature\Content\Content $content
 */
return function($content) {
    $content->addPageType('default');

    $content->addBlock('intro');
    $content->addBlock('gallery');
    $content->addBlock('title');
    $content->addBlock('mainText');

    // One-Liner, adding a new section and one article
    $content->addSection('page')->setPath('')->addArticleList([
        [
            'id'  => 'home',
            'tag' => ['route' => 'home'],
        ],
        ['id' => 'about'],
        ['id' => 'contact'],
        ['id' => 'socialmedia'],
    ]);

    // Fluent Interface Usage, adding a new section and multiple article
    $content->addSection('law')
            ->setPath('law/')
            ->addArticle('imprint', ['path' => 'law/imprint'])
            ->addArticle('privacy', ['path' => 'law/privacy']);

    // One-Liner, with passing an array of articles in a newly created section, uses path to specify a folder in which the data-files are
    $content->addSection('error')->setPath('error/')->addArticleList([
        [
            'id'   => '404',
            'file' => '404',
        ],
    ]);
};