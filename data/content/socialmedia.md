# SocialMedia

Demo page for social-media embeds with the Canal\Assets GDPR DoubleClick enabled, on Twitter and Google Maps are some more features shown.

See [PaintTheWeb - Canal Asset, Module GDPR](https://painttheweb.de/flood-component/canal-asset-module-gdpr) for implementation info.

> LazyLoad is currently enabled 

- Social
    - [Facebook](#anc--embed-facebook)
    - [Twitter](#anc--embed-twitter)
        - Example: feature auto_check_load
    - [Instagram Post](#anc--embed-instagram)
- Media/Fun
    - [Giphy](#anc--embed-gighy)
        - has checkbox, label before checkbox, button
    - [Pinterest](#anc--embed-pinterest)
         - has no checkbox, but label + button
    - [Tumblr](#anc--embed-tumblr) (def.)
    - [500px](#anc--embed-500px)
        - has no checkbox, only button
- Video
    - [YouTube](#anc--embed-youtube)
        - with native advanced privacy mode or default mode
    - [Vimeo](#anc--embed-vimeo)
- Audio
    - [Spotify](#anc--embed-spotify)
    - [Deezer](#anc--embed-deezer)
- Cloud Platforms
    - [Office365](#anc--embed-office365)
- Location Service
    - [Google Maps](#anc--embed-googlemaps)
        - Example: feature auto_check
- Dev. Pages
    - [JSFiddle](#anc--embed-jsfiddle)
    - [SassMeister](#anc--embed-sassmeister)
    - [CodePen](#anc--embed-codepen)
    - [GitHub Gist](#anc--embed-github)
- Other Pages/Embed Possibilities
    - [Fake Embed](#anc--embed-fakeembed)