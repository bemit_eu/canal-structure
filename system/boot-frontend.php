<?php

/**
 * @param $frontend \Flood\Canal\Frontend
 */
function bootFrontend($frontend) {
    $config = require dirname(__DIR__) . '/_config.php';
    $config($frontend);

    $frontend->init();

    $content = require dirname(__DIR__) . '/_content.php';
    $content($frontend->content);

    $route = require dirname(__DIR__) . '/_route.php';
    $route($frontend->route);
}