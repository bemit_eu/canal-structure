<?php

$request = filter_input(INPUT_SERVER, 'REQUEST_URI', FILTER_SANITIZE_URL);

if(false === $request) {
    error_log('Canal Server: REQUEST_URI is not valid.');
    exit(100);
}

if(null === $request) {
    error_log('Canal Server: REQUEST_URI is not set.');
    exit(101);
}

if(!is_string($request)) {
    error_log('Canal Server: unknown REQUEST_URI problem.');
    exit(102);
}

if(
    false === strpos($request, '/api/') && (
        preg_match('/\.(?:png|jpg|jpeg|gif)$/', $request) ||
        preg_match('/\.(?:pdf)$/', $_SERVER["REQUEST_URI"]) ||
        preg_match('/\.(?:eot|woff2|woff|ttf|svg)$/', $request)
    )
) {
    return false;
}

$filename = filter_input(INPUT_SERVER, 'REQUEST_URI', FILTER_UNSAFE_RAW);

if(false === $filename) {
    error_log('Canal Server: REQUEST_URI is not valid.');
    exit(110);
}
if(null === $filename) {
    error_log('Canal Server: REQUEST_URI is not set.');
    exit(111);
}

if(!is_string($filename)) {
    error_log('Canal Server: unknown REQUEST_URI problem.');
    exit(112);
}

if(0 === strpos($filename, '/data/out/') && preg_match('/\.(?:css|js|css\.map|js\.map)$/', $filename)) {
    $path = pathinfo('../' . $filename);
    if($path['extension'] == 'js') {
        header('Content-Type: application/javascript');
        readfile('../' . $filename);
        exit(0);
    }

    if($path['extension'] == 'css') {
        header('Content-Type: text/css');
        readfile('../' . $filename);
        exit(0);
    }

    if($path['extension'] == 'map') {
        readfile('../' . $filename);
        exit(0);
    }
}

if(php_sapi_name() == 'cli-server') {
    error_log('Canal: Running on the CLI SAPI');
}
require dirname(__DIR__) . '/flow.php';
